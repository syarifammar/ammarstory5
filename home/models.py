from django.db import models

# Create your models here.
class Jadwal(models.Model):
    kelas = models.CharField(max_length = 40) 
    deskripsi = models.TextField(default='')
    dosen = models.CharField(max_length = 40, default='') 
    ruang_kelas = models.CharField(max_length = 40, default='')
    jumlah_sks = models.IntegerField(default=3)
    tahun_ajaran = models.CharField(max_length = 40, default='')
    
    def __str__(self):
        return self.kelas
    

