from django import forms
from .models import Jadwal

##class JadwalForm(forms.Form):
    ##jadwal = forms.CharField(max_length=40,
    ##widget=forms.TextInput(
    ##    attrs={ 'class' : 'form-control', 'aria-describedby' : 'emailHelp', 'placeholder' : 'Masukkan Jadwal Kelas'}
    ##))
    
class JadwalForm(forms.ModelForm):
    

    class Meta:
        model = Jadwal
        fields = ['kelas', 'deskripsi', 'dosen', 'ruang_kelas','jumlah_sks', 'tahun_ajaran']
        pilihan_tahun_ajaran = [
        ('Gasal 2019/2020', 'Gasal 2019/2020'),
        ('Genap 2019/2020', 'Genap 2019/2020'),
        ('Gasal 2020/2021', 'Gasal 2020/2021'),
        ('Genap 2020/2021', 'Genap 2020/2021')
    ]

        widgets={
            'kelas' : forms.TextInput(attrs={ 'class' : 'form-control', 'aria-describedby' : 'emailHelp', 'placeholder' : 'Masukkan Jadwal Kelas'}),
            'deskripsi' : forms.TextInput(attrs={'class' : 'form-control',  'placeholder' : 'Apa yang ada di dalam mata kuliah ini?'}),
            'dosen' : forms.TextInput(attrs={'class' : 'form-control',  'placeholder' : 'Siapa dosenmu?'}),
            'ruang_kelas' : forms.TextInput(attrs={'class' : 'form-control',  'placeholder' : 'Ruang berapa?'}),
            'jumlah_sks' : forms.TextInput(attrs={'class' : 'form-control',  'placeholder' : 'Jumlah sks'}),
            'tahun_ajaran' : forms.Select(choices=pilihan_tahun_ajaran)
        }