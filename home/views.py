from django.shortcuts import render, redirect
from django.views.decorators.http import require_POST

from .models import Jadwal
from .forms import JadwalForm
# Create your views here.

def index(request):
    jadwal_kelas = Jadwal.objects.order_by('id')
    
    form = JadwalForm()

    context = {'jadwal_kelas' : jadwal_kelas, 'form' : form}
    
    if request.method == 'POST':
        form = JadwalForm(request.POST)

        if form.is_valid():
            new_jadwal = form.save()
            new_jadwal.save()

            return redirect('home:index')
        
    return render(request, 'home/index.html', context)

def liat(request, jadwal_id):
    jadwalnya = Jadwal.objects.get(id=jadwal_id)
    context = {'jadwalnya' : jadwalnya}
    return render(request, 'home/jadwal.html', context)

def delete(request, jadwal_id):
    Jadwal.objects.get(id=jadwal_id).delete()
    return redirect('home:index')