from django.urls import path

from . import views
app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('jadwal/<jadwal_id>', views.liat, name = 'jadwal'),
    path('delete/<jadwal_id>', views.delete, name = 'delete')
]